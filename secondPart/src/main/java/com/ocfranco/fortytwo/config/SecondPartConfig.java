package com.ocfranco.fortytwo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ocfranco.fortytwo.rabbitmq.SecondPartReceiver;

@Configuration
public class SecondPartConfig {

	@Bean
    public SecondPartReceiver receiver() {
        return new SecondPartReceiver();
    }
	
}
