package com.ocfranco.fortytwo.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.modelmapper.ModelMapper;

import com.ocfranco.fortytwo.entities.SecondPartEntity;
import com.ocfranco.fortytwo.pojos.PojoObject;

public class Utils {
	private static Utils singleton = null;
	public static Utils getInstance() {
		if (singleton == null) {
			synchronized (Utils.class) {
				if (singleton == null) {
					try {
						singleton = new Utils();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return singleton;
	}
	
	public Object byteToPojo(byte[] obj) throws IOException, ClassNotFoundException {
	    ByteArrayInputStream in = new ByteArrayInputStream(obj);
	    ObjectInputStream is = new ObjectInputStream(in);
	    return is.readObject();
	}
	
	public SecondPartEntity pojoToEntity(PojoObject pojoObject) {
		ModelMapper modelMapper = new ModelMapper();
	    SecondPartEntity entity = modelMapper.map(pojoObject, SecondPartEntity.class);
	    return entity;
	}
}
