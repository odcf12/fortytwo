package com.ocfranco.fortytwo.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ocfranco.fortytwo.pojos.PojoObject;
import com.ocfranco.fortytwo.rabbitmq.FirstPartSender;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="RecruitmentAssignment", description="FortyTwo Recruitment Assignment")
public class FirstPartController {
	private static final Logger logger = LogManager.getLogger(FirstPartController.class);

	@Autowired
	private FirstPartSender firstPartSender;
	
	@PostMapping("/api/fortytwo/")
	@ApiOperation(value = "Send Message to RabbitMQ")
	public void sendMessage(@RequestBody PojoObject pojoObject) throws IOException {
		logger.info("Start process sendMessage");
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);   
			out.writeObject(pojoObject);
			out.flush();
			byte[] pojoBytes = bos.toByteArray();
			firstPartSender.send(pojoBytes);
		}catch (Exception e) {
			logger.error("ERROR in process sendMessage" + e.getMessage());
			throw e;
		}finally {
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}
		logger.info("End process sendMessage");
	}
	
}
