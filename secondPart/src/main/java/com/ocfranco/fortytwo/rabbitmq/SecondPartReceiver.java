package com.ocfranco.fortytwo.rabbitmq;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import com.ocfranco.fortytwo.pojos.PojoObject;
import com.ocfranco.fortytwo.services.SecondPartService;
import com.ocfranco.fortytwo.utils.Utils;

@RabbitListener(queues = "fortytwoQueue")
public class SecondPartReceiver {
	private static final Logger logger = LogManager.getLogger(SecondPartReceiver.class);
	@Autowired
	private SecondPartService secondPartService;

	@RabbitHandler
    public void receive(byte[] object) {
		try {
			PojoObject pojoObject = (PojoObject)Utils.getInstance().byteToPojo(object);
			secondPartService.saveFortyTwo(pojoObject);
			logger.info("Message Received: " + pojoObject.toString());
		}catch (Exception e) {
			logger.error("Message Received: " + e.getMessage());
		}
    }
}
