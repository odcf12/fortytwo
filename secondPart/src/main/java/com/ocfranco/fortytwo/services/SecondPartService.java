package com.ocfranco.fortytwo.services;

import com.ocfranco.fortytwo.pojos.PojoObject;

public interface SecondPartService {
	public void saveFortyTwo(PojoObject pojoObject);
}
