package com.ocfranco.fortytwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages="com.ocfranco.fortytwo")
public class FirstPartApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstPartApplication.class, args);
	}

}

