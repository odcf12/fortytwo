package com.ocfranco.fortytwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondPartApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecondPartApplication.class, args);
	}

}

