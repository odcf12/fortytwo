package com.ocfranco.fortytwo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ocfranco.fortytwo.rabbitmq.FirstPartSender;


@Configuration
public class FirstPartConfig {

	@Bean
	public Queue fistPartQueue() {
		return new Queue("fortytwoQueue");
	}
	
	 @Bean
    public FirstPartSender sender() {
        return new FirstPartSender();
    }
	
}
