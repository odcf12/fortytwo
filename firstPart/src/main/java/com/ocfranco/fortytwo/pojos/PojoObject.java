package com.ocfranco.fortytwo.pojos;

import java.io.Serializable;

public class PojoObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String uuid;
	private String log;
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
	@Override
	public String toString() {
		return "PojoObject [uuid=" + uuid + ", log=" + log + "]";
	}
	
}
