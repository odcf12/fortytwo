package com.ocfranco.fortytwo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ocfranco.fortytwo.entities.SecondPartEntity;

public interface SecondPartRepository extends JpaRepository<SecondPartEntity,Integer>{

}
