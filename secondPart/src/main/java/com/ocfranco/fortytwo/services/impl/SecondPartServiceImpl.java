package com.ocfranco.fortytwo.services.impl;

import java.time.ZoneId;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ocfranco.fortytwo.entities.SecondPartEntity;
import com.ocfranco.fortytwo.pojos.PojoObject;
import com.ocfranco.fortytwo.repositories.SecondPartRepository;
import com.ocfranco.fortytwo.services.SecondPartService;
import com.ocfranco.fortytwo.utils.Utils;

@Service
public class SecondPartServiceImpl implements SecondPartService {

	@Autowired
	private SecondPartRepository secondPartRepository;
	
	@Override
	public void saveFortyTwo(PojoObject pojoObject) {
		SecondPartEntity entity = Utils.getInstance().pojoToEntity(pojoObject);
		entity.setCreated((new Date(System.currentTimeMillis()).toInstant()
			      .atZone(ZoneId.systemDefault())
			      .toLocalDateTime()));
		secondPartRepository.save(entity);
	}

}
