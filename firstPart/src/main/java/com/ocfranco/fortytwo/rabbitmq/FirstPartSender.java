package com.ocfranco.fortytwo.rabbitmq;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class FirstPartSender {
	private static final Logger logger = LogManager.getLogger(FirstPartSender.class);

	@Autowired
    private RabbitTemplate template;

    @Autowired
    private Queue queue;

    @Scheduled(fixedDelay = 1000, initialDelay = 500)
    public void send(byte[] pojoBytes) {
        this.template.convertAndSend(queue.getName(), pojoBytes);
        logger.info("Message Sent: " + pojoBytes.toString());
    }
}
